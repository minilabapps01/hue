﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMaker : MonoBehaviour
{
    [SerializeField] public WinLooseChecker winLooseChecker;
    [SerializeField] public MoveScript moveScript;
    [SerializeField] private GameObject gameModeEasy;
    [SerializeField] private GameObject gameModeHard;
    [SerializeField] private Sprite gameSprite;
    public List<GameObject> childs;
    public GameObject gameModeObj;
    void Start()
    {
        StartGame(gameModeHard);

    }

    public void StartGame(GameObject _gameMode)
    {
        if (gameModeObj != null)
        { 
            Destroy(gameModeObj);
        }
        gameModeObj = Instantiate(_gameMode); 
        int childcount = gameModeObj.transform.GetChild(1).childCount;
        for (int i = 0; i < childcount; i++)
        {
            gameModeObj.transform.GetChild(1).transform.GetChild(i).GetComponent<SpriteRenderer>().sprite = gameSprite;
        }
        moveScript.SetRings(gameModeObj);
        winLooseChecker.SetRings(gameModeObj);
    }

}
