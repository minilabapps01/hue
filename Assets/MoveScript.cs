﻿using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.EventSystems;

public class MoveScript : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    [SerializeField] WinLooseChecker winLooseChecker;
    [SerializeField] Transform choosenRingImage;
    public GameObject rings;
    bool canMove;
    Vector3 buf;
    Vector3 nowpos;
    Vector3 startDragPos;
    public GameObject choosenRing;
    int choosenRingid;
    string dragVector;


    public void SetRings(GameObject _rings)
    {
        rings = _rings;
        choosenRingid = 0;
        choosenRing = rings.transform.GetChild(1).GetChild(0).gameObject;
        choosenRingImage.localScale = rings.transform.GetChild(0).GetChild(0).localScale;
    }


    void Start()
    {
        canMove = true;
        choosenRingid = 0;
        choosenRing = rings.transform.GetChild(1).GetChild(choosenRingid).gameObject;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        startDragPos = eventData.position;
        if (Mathf.Abs(eventData.delta.x) > Mathf.Abs(eventData.delta.y))
        {
            dragVector = "H";
        }
        else
        {
            dragVector = "V";
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        nowpos = eventData.position;
        float distY = Mathf.Abs(startDragPos.y - nowpos.y);
        buf = startDragPos - nowpos;
        if (dragVector == "V")
        {
            ChooseRing(distY);
        }
        if (dragVector == "H")
        {
            RotateRing();
        }    
    }

    public void RotateRing()
    {
        if (choosenRing.GetComponent<SpriteInfo>().canMove)
        { 
            if (buf.x > 0)
            {
                choosenRing.transform.Rotate(new Vector3(0, 0, -1));
                startDragPos = nowpos;
            }
            if (buf.x < 0)
            {
                choosenRing.transform.Rotate(new Vector3(0, 0, 1));
                startDragPos = nowpos;
            }
        }
    }

    public void ChooseRing(float _distY)
    {
        if (buf.y > 0 && _distY > 100)
        {
            if (choosenRingid < rings.transform.GetChild(1).childCount - 1)
                choosenRingid++;
            startDragPos = nowpos;
        }
        if (buf.y < 0 && _distY > 100)
        {
            if (choosenRingid > 0)
                choosenRingid--;
            startDragPos = nowpos;
        }
        choosenRing = rings.transform.GetChild(1).GetChild(choosenRingid).gameObject;
        choosenRingImage.localScale = rings.transform.GetChild(0).GetChild(choosenRingid).localScale;
    }


    public void OnEndDrag(PointerEventData eventData)
    {
        if (choosenRing.GetComponent<SpriteInfo>().canMove)
        { 
            float rotZ = choosenRing.transform.rotation.z;
            if (rotZ < 0.07f && rotZ > -0.07f)
            {
                choosenRing.transform.rotation = new Quaternion(0, 0, 0, 0);
                choosenRing.GetComponent<SpriteInfo>().canMove = false;
                winLooseChecker.AddWin();
            }
        }
    }
}
