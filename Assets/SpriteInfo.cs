﻿using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SpriteInfo : MonoBehaviour
{
    public int randomStartPos;
    public bool canMove;

    void Start()
    {
        canMove = true;
        randomStartPos = UnityEngine.Random.Range(1, 37);
        transform.Rotate(new Vector3(0, 0, randomStartPos*10));
    }

}
