﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class WinLooseChecker : MonoBehaviour
{
    [SerializeField] MoveScript moveScript;
    public int winCount;
    int ringsCount;
    GameObject rings;
    public Text winText;

    public void SetRings(GameObject _rings)
    {
        rings = _rings;
        winCount = 0;
    }

    public void Start()
    {
        winCount = 0;
        try
        {
            winText.text = "";
        }
        catch
        {
            
        }
    }


    public void AddWin()
    {
        ringsCount = rings.transform.GetChild(1).childCount;
        winCount++;
        if (winCount == ringsCount - 1)
        {
            winText.text = "You win!!!";
        }
    }
}
